# Sexy Acrylic Keyboard

The first full-size keyboard I made and it doesn't have a good name yet. Handwired low profile acrylic ergonomic unibody split.

![Photo of the keyboard](https://gitlab.com/zyumbik/keyboard-1/-/raw/main/main-4.jpg)

## Features 

- 38 keys
- 4 encoders: two vertical and two horizontal EVQWGD001
- Running ZMK on Waveshare RP2040 Zero
- 3mm acrylic top and bottom designed in Figma (yes)
- Low profile Kailh Choc V1 light blue 20g switches
- Handwired using 1mm copper enameled wire and a bit of brass wire for the controller soldering
- RGB coming soon

Thumb cluster multi-press test/demo: https://youtu.be/icUilXmS68Y

Build inspiration: https://github.com/FSund/pteron-keyboard

## Case

You can find SVG and DXF files for laser cutting in this repo. In SVG, the size is 1px = 0.1mm. Both parts dimensions are 265.453 by 129.528 mm. 

The bottom plate features M2 threaded inserts (outer diameter 3.2mm). I put them in using soldering iron: T12 K tip. The holes were initially too small so I also widened them a bit with scissors. Note that these inserts have different orientation so you need to insert them one way only if you are gonna also use brass standoffs (maybe not all but mine do). That's why instead of brass standoffs I used M3 nuts as spacers.

There are extra holes in the bottom plate I thought would be needed to support the top plate better against flex. Turns out 3mm is hard enough and doesn't require any support. 18 mounting holes for M2 screws provide enough support.

## Mounting components

Switches are held in the plate using thin clear double-sided tape. Not very strong but it's the best method I found. Things I tried: thread seal tape, hot glue, thick clear double-sided tape, thin double-sided tape, liquid silicone sealant.

Horizontal encoders are mounted using hot glue. Vertical encoders are held from the bottom using headless screws.

## Handwiring

Don't use 1mm wire like I did! Use 0.8mm or even less if you want to wire to the controller directly. Here I had to use 0.4mm brass wire to solder the wire ends to the controller because 1mm is too thick. Note that the wire needs to be enameled otherwise you need other kind of insulation.

Matrix I wired is 8x5. An easier matrix would've been 10x4 but it takes 1 extra pin. The matrix allows for 40 switches and 38 of them are regular key switches. Each of the 4 encoders has a button so they wouldn't fit the matrix. I wired them in pairs (horizontal and vertical) to the same col/row. This means pressing the left roller encoder is the same as pressing the top vertical encoder. Same for right and bottom. 

![Wiring plan](https://gitlab.com/zyumbik/keyboard-1/-/raw/main/wiring-plan.png)

Wire wrapping was used extensively throughout this build. I use DIY wire-wrapping tools made of a pen, a cotton swab stick and a thick syringe needle. The rat's nest of hanging wires is for the future RGB support.

![Wiring photo](https://gitlab.com/zyumbik/keyboard-1/-/raw/main/wiring-1.jpeg)

![Wiring photo](https://gitlab.com/zyumbik/keyboard-1/-/raw/main/wiring-2.jpeg)

## Firmware

At first I wanted to use [KMK](https://github.com/KMKfw/kmk_firmware/) but with lots of combos I need it got very slow. So with the help of Pete I got [ZMK](https://zmk.dev/) working instead.  

ZMK setup and new board & shield definition + matrix transform and encoders: https://teletype.in/@gleb.sexy/zmk-setup

My shield: https://github.com/zyumbik/zmk-config/tree/main/config/boards/shields/sexy_acrylic

---

Figma file: https://figma.fun/whtKrb

Follow my work on Twitter: https://twitter.com/gleb_sexy/ 

Contact: https://gleb.sexy/#contact
